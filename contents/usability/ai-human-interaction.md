---
name: AI-human interaction
---

As Artificial Intelligence (AI) continues to advance, it brings both exciting opportunities and new challenges for product design. Although designing for AI still requires adhering to human-centered design principles, additional considerations such as ethics, privacy, trust, and transparency must be taken into account. The guidelines for AI-human interaction are crucial in ensuring that AI systems prioritize the user's needs, behave ethically, and safeguard the user's privacy, while also building trust in the technology.

## Guidelines

**Start with the user, not the technology.** AI technology should be leveraged to enhance the user experience, rather than be the primary focus. Design with a deep understanding of the user's needs, goals, and pain points. If you aren't aligned with a user's need, you are building a system that does not solve a problem. Instead of asking "Can we use AI to _____?", ask yourself "How might we help users _____?".

**Understand when to automate.** Understand if a task is a good fit for AI or if it is better done by a human. First, understand if a user's need will be helped by automation. Users may not want automation in high stakes tasks, tasks where they will be held responsible for the result, or tasks that they enjoy doing. Tasks that are a good fit for automation are tedious, error-prone, boring, low stakes, and free up the user's time. If users will benefit from automation, consider if the problem could be addressed with pre-defined rules (_if this, then that_). Understand the strengths and weaknesses of AI. AI can be helpful for processing large amounts of information, pattern finding, prediction, classification, and recommendations. Given good training data, AI can be more accurate and faster than a human at completing tasks. AI is less helpful for tasks requiring empathy, emotional intelligence, morality, common sense, predicability, contextual understanding, intuition, and creativity.

**Understand risk.** Understand the consequences to users if the system's recommendation is incorrect. Clarify the system's limitations in high stakes situations. If a high stakes feature is powered by AI, design for potential negative impact. For example, a user should explicitly opt-in to a high stakes AI-powered feature.

**Communicate confidence.** Users rely on the system to make decisions, but they should not trust the system entirely. Communicating confidence allows users to know how much scrutiny they should put recommendations under.

**Be transparent.** Establish trust by ensuring the user always knows when they are interacting with AI, when content was generated by AI, and when content or recommendations come from third parties. Users need to understand a system's capabilities and limits to understand how much trust to put into the system. Use clear, simple language to explain what the system is doing and how it arrived at its recommendations so that users can build a mental model of what the system is capable of. For example, explain what data the system is trained on and what it's optimized for. The user should also understand how their data is used and processed.

**Set the right expectations.** The interface should clearly communicate the AI's capabilities, limitations, and the scope of its decision-making authority.

**Give the user control** The user should be able to decide whether to follow the AI's recommendations or not. There should be an easy way to undo system actions. Do not collect user data without asking the user's permission.

**Fail gracefully.** When your system is not certain of the user's intent or has low confidence, make sure there is a path forward that does not rely on AI. Explain why the system was not able to provide a recommendation. Errors are also opportunities to learn more about your user's mental models and improve the system's ability to make recommendations. Consider designing a feedback mechanism that presents as a cue for adjustment rather than an error state.

**Encourage feedback.** Design mechanisms to collect implicit and explicit feedback to improve the system.

## Related patterns

- [Destructive actions](/usability/destructive-actions)
- [Saving and feedback](/usability/saving-and-feedback)
- [Contextual help and info](/usability/contextual-help)

## References

- [Guidelines for Human-AI Interaction, Microsoft](https://www.microsoft.com/en-us/research/uploads/prod/2019/01/Guidelines-for-Human-AI-Interaction-camera-ready.pdf)
- [People + AI Guidebook, Google](https://pair.withgoogle.com/guidebook)
