---
name: GitLab Voluntary Product Accessibility Template (VPAT®)
---

## Reports

- [GitLab Accessibility Conformance Report Revised Section 508 Edition](/accessibility/508)
- [GitLab Accessibility Conformance Report WCAG Edition](/accessibility/wcag)
